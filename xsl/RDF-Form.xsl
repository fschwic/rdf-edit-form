<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" version="1.0">

    <xsl:output method="html" encoding="iso-8859-1"/>

    <xsl:template match="/">
        <!-- documents have only one root element -->
        <xsl:apply-templates select="*[1]" mode="root"/>
    </xsl:template>

    <xsl:template match="rdf:RDF" mode="root">
        <!-- we support only one description -->
        <xsl:apply-templates select="*[1]" mode="root"/>
    </xsl:template>

    <xsl:template match="*" mode="root">
        <xsl:comment>Root is <xsl:value-of select="name()"/>, {<xsl:value-of
                select="namespace-uri()"/>}<xsl:value-of select="local-name()"/></xsl:comment>
        <form>
            <xsl:attribute name="id">
                <xsl:value-of select="namespace-uri()"/>
                <xsl:value-of select="local-name()"/>
            </xsl:attribute>
            <xsl:apply-templates/>
            <button type="submit">Save</button>
        </form>
    </xsl:template>

    <xsl:template match="*">
        <xsl:param name="parent-name"/>
        <div class="property">
            <xsl:attribute name="id">{<xsl:value-of select="namespace-uri()"/>}<xsl:value-of
                    select="local-name()"/></xsl:attribute>

            <xsl:choose>

                <xsl:when test="./*">
                    <xsl:apply-templates select="./*" mode="second-level"/>
                    <xsl:if test="@rdf:resource">
                        <span>Values related to <xsl:value-of select="@rdf:resource"/>.</span>
                    </xsl:if>
                    <xsl:apply-templates select="./*">
                        <xsl:with-param name="parent-name">
                            <xsl:if test="$parent-name"><xsl:value-of select="$parent-name"
                                /> - </xsl:if>
                            <xsl:value-of select="local-name()"/>
                        </xsl:with-param>
                    </xsl:apply-templates>
                </xsl:when>

                <xsl:otherwise>

                    <span class="label">
                        <xsl:if test="$parent-name"><xsl:value-of select="$parent-name"
                            /> - </xsl:if>
                        <xsl:value-of select="local-name()"/>
                    </span>

                    <xsl:if test="@rdf:resource and text()">
                        <xsl:comment>Found reference and text. Prefering reference.</xsl:comment>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="@rdf:resource">
                            <input class="value ref" type="text">
                                <xsl:attribute name="name">{<xsl:value-of select="namespace-uri()"
                                        />}<xsl:value-of select="local-name()"/></xsl:attribute>
                                <xsl:attribute name="value">
                                    <xsl:value-of select="@rdf:resource"/>
                                </xsl:attribute>
                            </input>
                        </xsl:when>
                        <xsl:when test="text()">
                            <input class="value" type="text">
                                <xsl:attribute name="name">{<xsl:value-of select="namespace-uri()"
                                        />}<xsl:value-of select="local-name()"/></xsl:attribute>
                                <xsl:attribute name="value">
                                    <xsl:value-of select="."/>
                                </xsl:attribute>
                            </input>
                        </xsl:when>
                        <xsl:otherwise>
                            <input class="value empty" type="text">
                                <xsl:attribute name="name">{<xsl:value-of select="namespace-uri()"
                                        />}<xsl:value-of select="local-name()"/></xsl:attribute>
                            </input>
                        </xsl:otherwise>
                    </xsl:choose>

                </xsl:otherwise>

            </xsl:choose>

        </div>
    </xsl:template>

    <xsl:template match="*" mode="second-level">
        <xsl:comment>Found second level field: {<xsl:value-of select="namespace-uri()"
                />}<xsl:value-of select="local-name()"/></xsl:comment>
    </xsl:template>

</xsl:stylesheet>
