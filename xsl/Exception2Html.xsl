<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:srw="http://www.loc.gov/zing/srw/"
    xmlns:sr="http://www.escidoc.de/schemas/searchresult/0.8"
    xmlns:prop="http://escidoc.de/core/01/properties/"
    xmlns:srel="http://escidoc.de/core/01/structural-relations/"
    xmlns:md="http://www.escidoc.de/schemas/metadatarecords/0.5"
    xmlns:ua="http://www.escidoc.de/schemas/useraccount/0.7"
    xmlns:pref="http://www.escidoc.de/schemas/preferences/0.1"
    xmlns:context="http://www.escidoc.de/schemas/context/0.7"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" version="1.0"
    xmlns:xlink="http://www.w3.org/1999/xlink">
    <xsl:output encoding="iso-8859-1" indent="yes" method="html"/>

    <!-- 
	 <title><h1>404 Context was not found.</h1></title>
	 <message><p>Object with id escidoc:16 does not exist!</p></message>
	 <class><p>de.escidoc.core.common.exceptions.application.notfound.ContextNotFoundException</p></class>
	 <stack-trace>
	 <cause><exception>...
    -->

    <xsl:template match="/exception">
      <div class="container-fluid">
	<div class="row-fluid">
	  <div class="span12">
	    <div class="hero-unit" id="titlePanel">
	      <h1><xsl:apply-templates select="title"/></h1>
	      <p><xsl:apply-templates select="class"/></p>
	      <h2><xsl:apply-templates select="message"/></h2>
	    </div>
	    <xsl:apply-templates select="stack-trace"/>
	    <xsl:apply-templates select="cause"/>
	  </div>
	</div>
      </div>
    </xsl:template>

    <xsl:template match="exception/title">
      Exception: <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="exception/message">
      <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="exception/class">
      <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="exception/stack-trace">
      <pre><xsl:value-of select="."/></pre>
    </xsl:template>

    <xsl:template match="exception/cause">
      <xsl:apply-templates select="exception"/>
    </xsl:template>

    <xsl:template match="exception">
	<h3><xsl:apply-templates select="title"/></h3>
	<p><xsl:apply-templates select="class"/></p>
	<p><xsl:apply-templates select="message"/></p>
      <xsl:apply-templates select="stack-trace"/>
      <xsl:apply-templates select="cause"/>
    </xsl:template>

</xsl:stylesheet>
