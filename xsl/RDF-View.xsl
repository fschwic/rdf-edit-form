<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" version="1.0">

    <xsl:output method="html" encoding="iso-8859-1"/>

    <xsl:template match="/">
        <!-- documents have only one root element -->
        <xsl:apply-templates select="*[1]" mode="root"/>
    </xsl:template>

    <xsl:template match="rdf:RDF" mode="root">
        <!-- we support only one description -->
        <xsl:apply-templates select="*[1]" mode="root"/>
    </xsl:template>

    <xsl:template match="*" mode="root">
        <xsl:comment>Root is <xsl:value-of select="name()"/>, {<xsl:value-of
                select="namespace-uri()"/>}<xsl:value-of select="local-name()"/></xsl:comment>
        <h2>{<xsl:value-of select="namespace-uri()"/>}<xsl:value-of select="local-name()"/></h2>
        <dl>
            <xsl:apply-templates/>
        </dl>
    </xsl:template>

    <xsl:template match="*">
        <xsl:param name="parent-name"/>
        <div class="property">
            <xsl:attribute name="id">{<xsl:value-of select="namespace-uri()"/>}<xsl:value-of
                    select="local-name()"/></xsl:attribute>

            <xsl:choose>

                <xsl:when test="./*">
                    <xsl:apply-templates select="./*" mode="second-level"/>
                    <xsl:if test="@rdf:resource">
                        <xsl:text>Values related to </xsl:text>
                        <xsl:value-of select="@rdf:resource"/>
                    </xsl:if>
                    <xsl:apply-templates select="./*">
                        <xsl:with-param name="parent-name">
                            <xsl:if test="$parent-name"><xsl:value-of select="$parent-name"
                                /> - </xsl:if>
                            <xsl:value-of select="local-name()"/>
                        </xsl:with-param>
                    </xsl:apply-templates>
                </xsl:when>

                <xsl:otherwise>
                    <dt class="key">
                        <xsl:if test="$parent-name"><xsl:value-of select="$parent-name"
                            /> - </xsl:if>
                        <xsl:value-of select="local-name()"/>
                    </dt>
                    <xsl:choose>
                        <xsl:when test="@rdf:resource and text()">
                            <xsl:comment>Found reference and text. Prefering reference.</xsl:comment>
                        </xsl:when>
                        <xsl:when test="@rdf:resource">
                            <dd class="value ref">
                                <xsl:value-of select="@rdf:resource"/>
                            </dd>
                        </xsl:when>
                        <xsl:when test="text()">
                            <dd class="value">
                                <xsl:value-of select="."/>
                            </dd>
                        </xsl:when>
                        <xsl:otherwise>
                            <dd class="value empty">
                                <xsl:text> </xsl:text>
                            </dd>
                        </xsl:otherwise>
                    </xsl:choose>

                </xsl:otherwise>

            </xsl:choose>

        </div>
    </xsl:template>

    <xsl:template match="*" mode="second-level">
        <xsl:comment>Found second level field: {<xsl:value-of select="namespace-uri()"
                />}<xsl:value-of select="local-name()"/></xsl:comment>
    </xsl:template>

</xsl:stylesheet>
